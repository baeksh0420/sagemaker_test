# import libraries
import boto3, re, sys, math, json, os, sagemaker, urllib.request
from sagemaker.amazon.amazon_estimator import get_image_uri
from sagemaker import get_execution_role

from IPython.display import Image
from IPython.display import display
from time import gmtime, strftime
from sagemaker.predictor import csv_serializer
import random
import numpy as np
import pandas as pd
import torch
import torch.optim as optim
import torch.nn.functional as F
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import re
import os
import joblib

torch.manual_seed(1)


# Define IAM role
role = get_execution_role()
my_region = boto3.session.Session().region_name # set the region of the instancer


device = 'cuda' if torch.cuda.is_available() else 'cpu'

random.seed(777)
torch.manual_seed(777)
if device == 'cuda':
  torch.cuda.manual_seed_all(777)




# S3에서 가져오는거로 변경해야함

#데이터 로드
xy = pd.read_csv('data/train_classi.csv', header=None)

xy=pd.DataFrame.dropna(xy, axis=0, how='any', thresh=None, subset=None, inplace=False)


x_data = xy.loc[1:,1:8]
y_data = xy.loc[1:, 9]



date = xy.loc[1:,0]
A = date.str.extract(r'(\d+)[:]', expand=True)
x_data["date1"] = A
x_data["date2"] = A


x_data = x_data.apply(pd.to_numeric)
y_data = y_data.apply(pd.to_numeric)

x_data = np.array(x_data)
y_data = np.array(y_data)



minMaxScaler = MinMaxScaler()
minMaxScaler.fit(x_data)
x_data = minMaxScaler.transform(x_data)



x_train=torch.FloatTensor(x_data)
y_train=torch.LongTensor(y_data)



from sklearn.ensemble import RandomForestClassifier

model = RandomForestClassifier(max_depth= 12, min_samples_leaf= 2, min_samples_split= 2, n_estimators = 3000, random_state=0)
model.fit(x_train, y_train)




